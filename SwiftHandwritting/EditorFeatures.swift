//
//  EditorFeatures.swift
//  SwiftHandwritting
//
//  Created by James Sumners on 12/29/19.
//  Copyright © 2019 James Sumners. All rights reserved.
//

import Foundation

class EditorFeatures: ObservableObject {
    enum UsedColor: Int {
        case black
        case blue
        case red
        case yellow
    }
    enum PenSize: Int {
        case small
        case medium
        case large
    }
    @Published public var undo: Bool
    @Published public var redo: Bool
    @Published public var touchType: InputMode
    @Published public var convert: Bool
    @Published public var penColor: UIColor
    @Published public var penColorValue: Int {
        didSet{
            switch penColorValue {
            case UsedColor.black.rawValue:
                self.penColor = .black
            case UsedColor.blue.rawValue:
                self.penColor = .blue
            case UsedColor.red.rawValue:
                self.penColor = .red
            case UsedColor.yellow.rawValue:
                self.penColor = .yellow
            default:
                self.penColor = .black
                print("Not a used color")
            }
        }
    }
    @Published public var touchValue: Int {
        didSet {
            if touchValue == 0 {
                self.touchType = .forcePen
            }
            else if touchValue == 1 {
                self.touchType = .forceTouch
            }
            else if touchValue == 2 {
                self.touchType = .auto
            }
        }
    }
    @Published public var penSize: PenSize
    @Published public var bibleRef = [String]()
    
    init() {
        self.undo = false
        self.redo = false
        self.touchType = .forcePen
        self.convert = false
        self.touchValue = 0
        self.penColor = .black
        self.penColorValue = 0
        self.penSize = .small
        
    }
    
}
