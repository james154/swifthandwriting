//
//  MyHandwritingView.swift
//  SwiftHandwritting
//
//  Created by James Sumners on 12/7/19.
//  Copyright © 2019 James Sumners. All rights reserved.
//

import SwiftUI

struct MyHandwritingView: View {
    let touchTypes = ["Pen", "Touch", "Auto"]
    let penColors = [Color.black, Color.blue, Color.red, Color.yellow]
    @State var penPicked = [true, false, false, false]
    @State var sizePicked = [true, false, false]
    @Environment(\.managedObjectContext) var moc
    @State var title: String = "Title"
    @EnvironmentObject var editorFeatures: EditorFeatures
//    @State var delegate: DocumentView?
    var body: some View {
        NavigationView {
            VStack {
                HStack{
                    Picker(selection: self.$editorFeatures.touchValue, label: Text("pick")) {
                        Image(systemName: "pencil.tip").tag(0)
                        Image(systemName: "hand.point.left").tag(1)
                        Text("Auto").tag(2)
                    }.pickerStyle(SegmentedPickerStyle())
                    .fixedSize()
                        .offset(x: CGFloat(15))
                    
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack(spacing: 10) {
                            Button(action: {
                                self.editorFeatures.penColorValue = 0
                                self.pickColor(forPen: 0)
                            }){
                                PenColorView(color: self.penColors[0], size: 12, highlight: self.penPicked[0])
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 1
                                self.pickColor(forPen: 1)
                            }){
                                PenColorView(color: self.penColors[1], size: 12, highlight: self.penPicked[1])
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 2
                                
                                self.pickColor(forPen: 2)
                            }){
                                PenColorView(color: self.penColors[2], size: 12, highlight: self.penPicked[2])
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 3
                                self.pickColor(forPen: 3)
                            }){
                                PenColorView(color: self.penColors[3], size: 12, highlight: self.penPicked[3])
                            }
                            
                        }
                    }
                    .frame(maxWidth: CGFloat(40))
                    .offset(x: CGFloat(30))
                    
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack(spacing: 10) {
                            Button(action: {
                                self.editorFeatures.penSize = .small
                                self.pickSize(forPen: 0)
                            }){
                                PenColorView(color: self.penColors[self.editorFeatures.penColorValue], size: 3, highlight: self.sizePicked[0])
                            }
                            Button(action: {
                                self.editorFeatures.penSize = .medium
                                self.pickSize(forPen: 1)
                            }){
                                PenColorView(color: self.penColors[self.editorFeatures.penColorValue], size: 6, highlight: self.sizePicked[1])
                            }
                            Button(action: {
                                self.editorFeatures.penSize = .large
                                self.pickSize(forPen: 2)
                            }){
                                PenColorView(color: self.penColors[self.editorFeatures.penColorValue], size: 9, highlight: self.sizePicked[2])
                            }
                            
                        }
                    }
                    .frame(maxWidth: CGFloat(40))
                    .offset(x: CGFloat(30))
                    
                    if self.editorFeatures.bibleRef.count > 0
                    {
                        Button(action: {
                            let bcv = BookChapterVerse(string: self.editorFeatures.bibleRef.last ?? "NO REF")
                            if let url = URL(string: bcv.bibleReferences[0].link()) {
                                UIApplication.shared.open(url)
                                print("Go to reference")
                            }
                            else
                            {
                                print("That url doesn't work")
                            }
                        }) {
                            Text("\(self.editorFeatures.bibleRef.last ?? "NO REF" )")
                        }
                        .fixedSize()
                            
                            .contextMenu{
                                    ForEach(0..<self.editorFeatures.bibleRef.count, id: \.self) { ref in
                                        Button(action: {
                                            let bcv = BookChapterVerse(string: self.editorFeatures.bibleRef[ref])
                                            if let url = URL(string: bcv.bibleReferences[0].link()) {
                                                UIApplication.shared.open(url)
                                                print("Go to reference")
                                            }
                                            else
                                            {
                                                print("That url doesn't work")
                                            }
                                        }) {
                                            Text("\(self.editorFeatures.bibleRef[ref] )")
                                        }.offset(x: CGFloat(30))
                                    }
                                }
                                .offset(x: CGFloat(30))
                    
                            

                    }
                    else
                    {
                        Text("NO REF").offset(x: CGFloat(30))
                    }
                    
                    
                    HStack{
                    Spacer()
                    Button(action: {
                        self.editorFeatures.undo = true
                        print("SwiftUI UNDO \(self.editorFeatures.undo)")
                    }){
                        Image(systemName: "arrow.uturn.left").font(.system(size: 22))
                    }
                    .offset(x: CGFloat(-10))
                    Button(action: {
                        self.editorFeatures.redo = true
                        print("SwiftUI REDO \(self.editorFeatures.redo)")
                    }){
                        Image(systemName: "arrow.uturn.right").font(.system(size: 22))
                    }
                    .offset(x: CGFloat(-15))
                    }
                }
                
                HandwrittingViewController(title: self.title, delegate: self).environmentObject(editorFeatures)
                
            }

            .navigationBarTitle(self.title)
            .navigationBarItems(trailing:
                                Button(action: {
                                    self.editorFeatures.convert = true
                                }){
                                    Text("Convert")
                                })
            
            
        }
        .environment(\.horizontalSizeClass, .compact)
    }
    
    func pickColor(forPen picked: Int) {
        for i in 0 ..< self.penPicked.count {
            if i == picked {
                self.penPicked[i] = true
            }
            else {
                self.penPicked[i] = false
            }
        }
    }
    
    func pickSize(forPen picked: Int) {
        for i in 0 ..< self.sizePicked.count {
            if i == picked {
                self.sizePicked[i] = true
            }
            else {
                self.sizePicked[i] = false
            }
        }
    }
    
    func resetUndo()
    {
        self.editorFeatures.undo = false
    }
    
    func undo() -> Bool
    {
        return self.editorFeatures.undo
    }
    
    func resetRedo()
    {
        self.editorFeatures.redo = false
    }
    
    func redo() -> Bool
    {
        return self.editorFeatures.redo
    }
}

struct HandwrittingViewController: UIViewControllerRepresentable{
    @EnvironmentObject var editorFeatures: EditorFeatures
    @State var title: String
    @State var delegate: MyHandwritingView
    typealias UIViewControllerType = EditorViewController
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<HandwrittingViewController>) -> EditorViewController {
        let editorViewController = EditorViewController()
        editorViewController.view.isUserInteractionEnabled = true
//        editorViewController.di
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if (appDelegate.engine == nil)
            {
                print("Certificate error \(appDelegate.engineErrorMessage ?? "No ERROR")")
            }
            editorViewController.engine = appDelegate.engine
        }

        // Configure the iink runtime environment
        let configurationPath = Bundle.main.bundlePath.appending("/recognition-assets/conf")
        do {
            try editorViewController.engine.configuration.setStringArray([configurationPath], forKey:"configuration-manager.search-path") // Tells the engine where to load the recognition assets from.
        } catch {
            print("Should not happen, please check your resources assets : " + error.localizedDescription)
        }
        
        
        // Set the temporary directory
        do {
            try editorViewController.engine.configuration.setString(NSTemporaryDirectory(), forKey: "content-package.temp-folder")
        } catch {
            print("Failed to set temporary folder: " + error.localizedDescription)
        }
        
        editorViewController.inputMode = self.editorFeatures.touchType
        
        do {
            if let package = try createPackage(packageName: "New") {
                let myText = NSAttributedString(string: """
Test inputing text.
""")
                
                
                try editorViewController.editor.part = package.getPartAt(0)
                
                // This works when createPackage is returning a "Text" block.
                try editorViewController.editor.import_(IINKMimeType.text, data: myText.string, block: nil)
                
                // Does not work when createPackage is returning "Text Document".
//                try editorViewController.editor.addBlock(.zero, type: "Text", mimeType: .text, data: myText.string)
                
                // This also does not work on Text Documents
//                let tempVal = try editorViewController.editor.addBlock(.zero, type: "Text")
//                try editorViewController.editor.import_(IINKMimeType.text, data: myText.string, block: tempVal)
                
            }
        } catch {
            print("Error while creating package : " + error.localizedDescription)
        }
        
        return editorViewController
    }
    
    // MARK: - Create package

    func createPackage(packageName: String) throws -> IINKContentPackage? {
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = (UIApplication.shared.delegate as? AppDelegate)?.engine {
            resultPackage = try engine.createPackage(fullPath.decomposedStringWithCanonicalMapping)
            
            // Add a blank page type Text Document
            if let part = try resultPackage?.createPart("Text") /* Options are : "Diagram", "Drawing", "Math", "Text Document", "Text" */ {
                self.title = "Type: " + part.type
            }
        }
        return resultPackage
    }
    
    func updateUIViewController(_ editorViewController: EditorViewController, context: UIViewControllerRepresentableContext<HandwrittingViewController>) {
        
        if editorFeatures.undo {
            editorViewController.editor.undo()
            self.editorFeatures.undo = false
        }
        else if editorFeatures.redo {
            editorViewController.editor.redo()
            self.editorFeatures.redo = false
            
        }
        else if editorFeatures.convert {
            do {
                
                let supportedTargetStates = editorViewController.editor.getSupportedTargetConversionState(nil)
                try editorViewController.editor.convert(nil, targetState: supportedTargetStates[0].value)
//                self.delegate.delegate!.saveText(NSAttributedString(string: try editorViewController.editor.export_(nil, mimeType: .text)))
            }
            catch {
                
                print("Error while converting handwriting : " + error.localizedDescription)
                
            }
            self.editorFeatures.convert = false
        }
        
        editorViewController.inputMode = editorFeatures.touchType
        var penSize = "0.5"
        switch editorFeatures.penSize {
        case .small:
            penSize = "0.2"
        case .medium:
            penSize = "0.5"
        case .large:
            penSize = "0.8"
        }
        
        if editorFeatures.penColor == UIColor.black {
            editorViewController.editor.penStyle = "color: #000000FF; -myscript-pen-width: "+penSize
        }
        else if editorFeatures.penColor == UIColor.blue {
            editorViewController.editor.penStyle = "color: #0000FFFF; -myscript-pen-width: "+penSize
        }
        else if editorFeatures.penColor == UIColor.red {
            editorViewController.editor.penStyle = "color: #FF0000FF; -myscript-pen-width: "+penSize
        }
        else if editorFeatures.penColor == UIColor.yellow {
            editorViewController.editor.penStyle = "color: #FFFF00FF; -myscript-pen-width: "+penSize
        }
        
        do{

            let processString = try editorViewController.editor.export_(nil, mimeType: .text)
            let refFound = BookChapterVerse(string: processString)
            if refFound.bibleReferences.count > 0 && refFound.bibleReferences.count != self.editorFeatures.bibleRef.count {
                self.editorFeatures.bibleRef.removeAll()
                for ref in 0..<refFound.bibleReferences.count {
                    self.editorFeatures.bibleRef.append(refFound.bibleReferences[ref].reference())
                }
            }
            
        }
        catch{
            print("Error while exproting string : " + error.localizedDescription)
        }
        
        
        print("updateUIViewController called")
        
    }
    
    func makeCoordinator() -> HandwrittingViewController.Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, IINKEditorDelegate, ObservableObject {
        
        var parent: HandwrittingViewController
        
        init(_ editorViewController: HandwrittingViewController) {
            parent = editorViewController
            print("Coord init")
        }
        
        func partChanged(_ editor: IINKEditor) {
            print("part")
        }
        
        func contentChanged(_ editor: IINKEditor, blockIds: [String]) {
            print("content")
        }
        
        func onError(_ editor: IINKEditor, blockId: String, message: String) {
            print("error")
        }
        
        
        
    }
    
}

//extension EditorViewController {
//
//    @objc func changeFromParent(sender: HandwrittingViewController) {
//        if self.delegate.undoCall {
//            uiViewController.editor.undo()
//            self.delegate.undoCall = false
//        }
//        else if self.delegate.redoCall {
//            uiViewController.editor.redo()
//            self.delegate.redoCall = false
//
//        }
//    }
//}

struct MyHandwritingView_Previews: PreviewProvider {
    static var previews: some View {
        MyHandwritingView()
    }
}
