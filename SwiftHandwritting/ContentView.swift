//
//  ContentView.swift
//  SwiftHandwritting
//
//  Created by James Sumners on 12/7/19.
//  Copyright © 2019 James Sumners. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    var body: some View {
        //MyHandwritingView().environment(\.managedObjectContext, self.moc)
        VCSwiftUIView(storyboard: "MyScript", VC: "InitVC")
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
