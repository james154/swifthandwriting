//
//  BibleReference.swift
//  BibleRefParsing
//
//  Created by James Sumners on 6/29/19.
//  Copyright © 2019 James Sumners. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    
    public func setAsLink(linkURL:String, foundRange:NSRange) -> Bool {
        
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

class BibleReference {
    
    enum BIBLE_APP: Int, CaseIterable {
        case LOGOS
        case OLIVETREE
        case YOU_VERSION
        
        var description: String {
            switch self {
            case .LOGOS:
                return "Logos"
            case .OLIVETREE:
                return "Olivetree"
            case .YOU_VERSION:
                return "YouVersion"
            }
        }
        
    }
    
    private let versePattern = "([\\d](\\s*))?(\\b[a-zA-Z]\\w+\\s\\d+)(:\\d+)+([-–]\\d+)?([,;](\\s)?(\\d+:)?\\d+([-–]\\d+)?)?"
    
    
    private var PreferredAPP = BIBLE_APP(rawValue: UserDefaults.standard.integer(forKey: "BibleApp")) ?? BIBLE_APP.LOGOS
    
    var searchText = NSMutableAttributedString()
    
    init(attributedText: NSAttributedString){
        searchText = NSMutableAttributedString(attributedString: attributedText)
    }
    
    public func preferredApp(pApp: BIBLE_APP)
    {
        PreferredAPP = pApp
    }
    
    public func returnAppUrl(bibleRef: BookChapterVerse.BibleReferences) -> String
    {
        var returnLink: String = ""
        let youVersion = YouVersion(string: bibleRef.book)
        if (PreferredAPP == .LOGOS)
        {
            returnLink = "logosref:Bible."
            let book = bibleRef.book.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
            returnLink = returnLink + book + "." + String(bibleRef.chapter) + "." + String(bibleRef.verse)
        }
        else if (PreferredAPP == .OLIVETREE)
        {
            let tempStr = "olivetree://bible/"+"\(bibleRef.book) \(bibleRef.chapter):\(bibleRef.verse)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
            returnLink = tempStr
        }
        else if (PreferredAPP == .YOU_VERSION)
        {
            returnLink = "youversion://bible?&reference="
            let book = youVersion.abbreviation//bibleRef.book.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
            returnLink = returnLink + book + "." + String(bibleRef.chapter) + "." + String(bibleRef.verse)
        }
        
        if true
        {
            print("\(bibleRef.book) \(bibleRef.chapter):\(bibleRef.verse)\n\(searchText.attributedSubstring(from: bibleRef.range).string)")
            print(returnLink)
        }
        
        return returnLink
    }
    
    public func setSearchText(setText: NSAttributedString)
    {
        searchText = NSMutableAttributedString(attributedString: setText)
    }
    
    public func getSearchText() -> NSAttributedString
    {
        return searchText
    }
    
    public func linkedAttributedString() -> NSAttributedString
    {
        let refFound = BookChapterVerse(string: searchText.string)
        
        if refFound.bibleReferences.count > 0
        {
            for reference in refFound.bibleReferences {
//                var hasLink = false
                
                let linkUrl = self.returnAppUrl(bibleRef: reference)
//                let attStr = searchText.attributedSubstring(from: reference.range)
//                let attribs = attStr.attributes(at: 0,effectiveRange: nil)
//                //                print("*******Start Attribute Print *********")
//                for attr in attribs {
//                    //                  print(attr.key, attr.value)
//                    if (attr.key == NSAttributedString.Key.link)
//                    {
//                        hasLink = true
//                    }
//                }
                //                print("*******End Attribute Print *********")
                
//                if (hasLink == false){
                    _ = searchText.setAsLink(linkURL: linkUrl, foundRange: reference.range)
//                }
            }
        }
        
//
//        var index1st = 0
//        let uiTextLen = searchText.length
//        var indexEnd = uiTextLen
//
//        while index1st < uiTextLen {
//            let searchRange = NSMakeRange(index1st, indexEnd)
//
//            let findPosition = searchText.mutableString.range(of: versePattern, options: .regularExpression, range: searchRange)
//
//
//            if (findPosition.length > 0)
//            {
//                var haslink = false
//                let foundText = searchText.mutableString.substring(with: findPosition)
//                let linkUrl = self.returnAppUrl(bibleRef: String(foundText))
//                let attStr = searchText.attributedSubstring(from: searchRange)
//                let attribs = attStr.attributes(at: 0, effectiveRange: nil)
//                //                print("*******Start Attribute Print *********")
//                for attr in attribs {
//                    //                  print(attr.key, attr.value)
//                    if (attr.key == NSAttributedString.Key.link)
//                    {
//                        haslink = true
//                    }
//                }
//                //                print("*******End Attribute Print *********")
//
//                if (haslink == false){
//                    _ = searchText.setAsLink(textToFind: String(foundText), linkURL: linkUrl, foundRange: findPosition)
//                }
//
//                _ = BookChapterVerse(string: foundText)
//
//            }
//
//            index1st = findPosition.location + findPosition.length
//            indexEnd = uiTextLen - index1st
//        }
//
        return searchText
    }
    
}
