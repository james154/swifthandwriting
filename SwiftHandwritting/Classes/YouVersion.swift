//
//  YouVersion.swift
//  BibleScribe
//
//  Created by James Sumners on 2/8/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import Foundation

class YouVersion {
    
    public let youVersion = ["1 Timothy" : "1TI",
    "2 Thessalonians" : "2TH",
    "Esther" : "EST",
    "Deuteronomy" : "DEU",
    "Isaiah" : "ISA",
    "Ruth" : "RUT",
    "2 Samuel" : "2SA",
    "Joshua" : "JOS",
    "Haggai" : "HAG",
    "2 Corinthians" : "2CO",
    "Jonah" : "JON",
    "Joel" : "JOL",
    "Daniel" : "DAN",
    "Leviticus" : "LEV",
    "Judges" : "JDG",
    "James" : "JAS",
    "Hosea" : "HOS",
    "Zechariah" : "ZEC",
    "Obadiah" : "OBA",
    "Jeremiah" : "JER",
    "2 Chronicles" : "2CH",
    "2 Peter" : "2PE",
    "1 John" : "1JN",
    "Acts" : "ACT",
    "2 Kings" : "2KI",
    "Exodus" : "EXO",
    "Habakkuk" : "HAB",
    "Numbers" : "NUM",
    "Ezra" : "EZR",
    "Jude" : "JUD",
    "1 Kings" : "1KI",
    "Ecclesiastes" : "ECC",
    "Philemon" : "PHM",
    "3 John" : "3JN",
    "Lamentations" : "LAM",
    "Zephaniah" : "ZEP",
    "Proverbs" : "PRO",
    "Colossians" : "COL",
    "Romans" : "ROM",
    "Micah" : "MIC",
    "1 Samuel" : "1SA",
    "Luke" : "LUK",
    "Malachi" : "MAL",
    "Job" : "JOB",
    "1 Chronicles" : "1CH",
    "Ephesians" : "EPH",
    "Mark" : "MRK",
    "Amos" : "AMO",
    "Matthew" : "MAT",
    "1 Thessalonians" : "1TH",
    "Nahum" : "NAM",
    "2 John" : "2JN",
    "Song of Solomon" : "SNG",
    "Genesis" : "GEN",
    "1 Peter" : "1PE",
    "Ezekiel" : "EZK",
    "Hebrews" : "HEB",
    "Revelation" : "REV",
    "Psalms" : "PSA",
    "2 Timothy" : "2TI",
    "1 Corinthians" : "1CO",
    "Galatians" : "GAL",
    "Philippians" : "PHP",
    "John" : "JHN",
    "Titus" : "TIT",
    "Nehemiah" : "NEH"]
    
    public var abbreviation: String = ""
    
    init(string: String) {
        abbreviation = youVersion[string] ?? string
    }
}
