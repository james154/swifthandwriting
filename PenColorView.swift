//
//  PenColorView.swift
//  SwiftHandwritting
//
//  Created by James Sumners on 1/1/20.
//  Copyright © 2020 James Sumners. All rights reserved.
//

import SwiftUI

struct PenColorView: View {
    var color: Color = Color.black
    var size: CGFloat = 15
    var highlight: Bool = false
    var body: some View {
        ZStack{
            if self.highlight {
                Circle()
                    .fill(Color.gray)
                    .frame(width: size+4, height: size+4)
            }
            Circle()
                .fill(color)
                .frame(width: size, height: size)
        }
        
    }
}

struct PenColorView_Previews: PreviewProvider {
    static var previews: some View {
        PenColorView()
    }
}
