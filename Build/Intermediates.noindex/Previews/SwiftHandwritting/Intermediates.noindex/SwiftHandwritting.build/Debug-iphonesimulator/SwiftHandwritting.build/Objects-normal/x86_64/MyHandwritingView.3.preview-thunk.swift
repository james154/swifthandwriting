@_private(sourceFile: "MyHandwritingView.swift") import SwiftHandwritting
import SwiftUI
import SwiftUI

extension MyHandwritingView_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 280)
        AnyView(__designTimeSelection(MyHandwritingView(), "#5527.[3].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
typealias Coordinator = HandwrittingViewController.Coordinator
typealias UIViewControllerType = HandwrittingViewController.UIViewControllerType

    @_dynamicReplacement(for: onError(_:blockId:message:)) private func __preview__onError(_ editor: IINKEditor, blockId: String, message: String) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 254)
            __designTimeSelection(print(__designTimeString("#5527.[2].[8].[4].[0].arg[0].value.[0].value", fallback: "error")), "#5527.[2].[8].[4].[0]")
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
    @_dynamicReplacement(for: contentChanged(_:blockIds:)) private func __preview__contentChanged(_ editor: IINKEditor, blockIds: [String]) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 250)
            __designTimeSelection(print(__designTimeString("#5527.[2].[8].[3].[0].arg[0].value.[0].value", fallback: "content")), "#5527.[2].[8].[3].[0]")
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
    @_dynamicReplacement(for: partChanged(_:)) private func __preview__partChanged(_ editor: IINKEditor) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 246)
            __designTimeSelection(print(__designTimeString("#5527.[2].[8].[2].[0].arg[0].value.[0].value", fallback: "part")), "#5527.[2].[8].[2].[0]")
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: makeCoordinator()) private func __preview__makeCoordinator() -> HandwrittingViewController.Coordinator {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 233)
        __designTimeSelection(Coordinator(__designTimeSelection(self, "#5527.[2].[7].[0].arg[0].value")), "#5527.[2].[7].[0]")
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: updateUIViewController(_:context:)) private func __preview__updateUIViewController(_ editorViewController: EditorViewController, context: UIViewControllerRepresentableContext<HandwrittingViewController>) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 189)
        if editorFeatures.undo {
            __designTimeSelection(editorViewController.editor.undo(), "#5527.[2].[6].[0].[0].[0]")
            self.editorFeatures.undo = false
        }
        else if editorFeatures.redo {
            __designTimeSelection(editorViewController.editor.redo(), "#5527.[2].[6].[0].[1].[0]")
            self.editorFeatures.redo = false
            
        }
        else if editorFeatures.convert {
            do {
                
                let supportedTargetStates = editorViewController.editor.getSupportedTargetConversionState(nil)
                try editorViewController.editor.convert(nil, targetState: supportedTargetStates[0].value)
//                self.delegate.delegate!.saveText(NSAttributedString(string: try editorViewController.editor.export_(nil, mimeType: .text)))
            }
            catch {
                
                print("Error while converting handwriting : " + error.localizedDescription)
                
            }
            self.editorFeatures.convert = false
        }
        
        editorViewController.inputMode = editorFeatures.touchType
        
        if editorFeatures.penColor == UIColor.black {
            editorViewController.editor.penStyle = "color: #000000FF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.blue {
            editorViewController.editor.penStyle = "color: #0000FFFF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.red {
            editorViewController.editor.penStyle = "color: #FF0000FF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.yellow {
            editorViewController.editor.penStyle = "color: #FFFF00FF; -myscript-pen-width: 1.0"
        }
        
        __designTimeSelection(print(__designTimeString("#5527.[2].[6].[3].arg[0].value.[0].value", fallback: "updateUIViewController called")), "#5527.[2].[6].[3]")
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: createPackage(packageName:)) private func __preview__createPackage(packageName: String) throws -> IINKContentPackage? {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 175)
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = (UIApplication.shared.delegate as? AppDelegate)?.engine {
            resultPackage = try engine.createPackage(fullPath.decomposedStringWithCanonicalMapping)
            
            // Add a blank page type Text Document
            if let part = try resultPackage?.createPart("Text") /* Options are : "Diagram", "Drawing", "Math", "Text Document", "Text" */ {
                           self.title = "Type: " + part.type
            }
        }
        return __designTimeSelection(resultPackage, "#5527.[2].[5].[3]")
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: makeUIViewController(context:)) private func __preview__makeUIViewController(context: UIViewControllerRepresentableContext<HandwrittingViewController>) -> EditorViewController {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 125)
        let editorViewController = EditorViewController()
        editorViewController.view.isUserInteractionEnabled = true
//        editorViewController.di
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if (appDelegate.engine == nil)
            {
                __designTimeSelection(print("\(__designTimeString("#5527.[2].[4].[2].[0].[0].[0].[0].arg[0].value.[0].value", fallback: "Certificate error "))\(appDelegate.engineErrorMessage ?? "No ERROR")\(__designTimeString("#5527.[2].[4].[2].[0].[0].[0].[0].arg[0].value.[2].value", fallback: ""))"), "#5527.[2].[4].[2].[0].[0].[0].[0]")
            }
            editorViewController.engine = appDelegate.engine
        }

        // Configure the iink runtime environment
        let configurationPath = Bundle.main.bundlePath.appending(__designTimeString("#5527.[2].[4].[3].value.modifier[0].arg[0].value.[0].value", fallback: "/recognition-assets/conf"))
        do {
            try editorViewController.engine.configuration.setStringArray([configurationPath], forKey:"configuration-manager.search-path") // Tells the engine where to load the recognition assets from.
        } catch {
            print("Should not happen, please check your resources assets : " + error.localizedDescription)
        }
        
        
        // Set the temporary directory
        do {
            try editorViewController.engine.configuration.setString(NSTemporaryDirectory(), forKey: "content-package.temp-folder")
        } catch {
            print("Failed to set temporary folder: " + error.localizedDescription)
        }
        
        editorViewController.inputMode = self.editorFeatures.touchType
        
        do {
            if let package = try createPackage(packageName: "New") {
                let myText = NSAttributedString(string: "Hello, MyScript")
                
                
                try editorViewController.editor.part = package.getPartAt(0)
//                try editorViewController.editor.addBlock(.zero, type: "Text", mimeType: .text, data: "Test")
                try editorViewController.editor.import_(IINKMimeType.text, data: myText.string, block: nil)
            }
        } catch {
            print("Error while creating package : " + error.localizedDescription)
        }
        
        return __designTimeSelection(editorViewController, "#5527.[2].[4].[8]")
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: redo()) private func __preview__redo() -> Bool {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 114)
        return __designTimeSelection(self.editorFeatures.redo, "#5527.[1].[9].[0]")
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: resetRedo()) private func __preview__resetRedo() {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 109)
        self.editorFeatures.redo = false
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: undo()) private func __preview__undo() -> Bool {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 104)
        return __designTimeSelection(self.editorFeatures.undo, "#5527.[1].[7].[0]")
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: resetUndo()) private func __preview__resetUndo() {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 99)
        self.editorFeatures.undo = false
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 19)
        AnyView(__designTimeSelection(NavigationView {
            __designTimeSelection(VStack {
                __designTimeSelection(HStack{
                    __designTimeSelection(Picker(selection: __designTimeSelection(self.$editorFeatures.touchValue, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[0].value"), label: __designTimeSelection(Text(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[1].value.arg[0].value.[0].value", fallback: "pick")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[1].value")) {
                        __designTimeSelection(ForEach(0 ..< self.touchTypes.count) {
                            __designTimeSelection(Text(__designTimeSelection(self.touchTypes[__designTimeSelection($0, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[2].value.[0].arg[1].value.[0].arg[0].value.[0].value")], "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[2].value.[0].arg[1].value.[0].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[2].value.[0].arg[1].value.[0]")
                        }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[2].value.[0]")
                    }.pickerStyle(__designTimeSelection(SegmentedPickerStyle(), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].modifier[0].arg[0].value"))
                    .fixedSize()
                        .offset(x: __designTimeSelection(CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].modifier[2].arg[0].value.arg[0].value", fallback: 30)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].modifier[2].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0]")
                    
//                    Picker(selection: self.$editorFeatures.penColorValue, label: Text("pick")) {
//                        ForEach(0 ..< self.penColors.count) {
//                            PenColorView(color: self.penColors[$0], size: 12)
//                        }
//                    }.pickerStyle(SegmentedPickerStyle())
//                    .fixedSize()
//                    .offset(x: CGFloat(30))
                    __designTimeSelection(ScrollView(.horizontal){
                        __designTimeSelection(HStack(spacing: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value", fallback: 10)) {
                            __designTimeSelection(Button(action: {
                                self.editorFeatures.penColorValue = 0
                            }){
                                __designTimeSelection(PenColorView(color: __designTimeSelection(self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: 0)], "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value"), size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[1].value", fallback: 12)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0]")
                            }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0]")
                            __designTimeSelection(Button(action: {
                                self.editorFeatures.penColorValue = 1
                            }){
                                __designTimeSelection(PenColorView(color: __designTimeSelection(self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: 1)], "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0].arg[0].value"), size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0].arg[1].value", fallback: 12)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0]")
                            }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1]")
                            __designTimeSelection(Button(action: {
                                self.editorFeatures.penColorValue = 2
                            }){
                                __designTimeSelection(PenColorView(color: __designTimeSelection(self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: 2)], "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[0].value"), size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[1].value", fallback: 12)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0]")
                            }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2]")
                            __designTimeSelection(Button(action: {
                                self.editorFeatures.penColorValue = 3
                            }){
                                __designTimeSelection(PenColorView(color: __designTimeSelection(self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0].arg[0].value.[0].value", fallback: 3)], "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0].arg[0].value"), size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0].arg[1].value", fallback: 12)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0]")
                            }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3]")
                            
                        }
                        .background(__designTimeSelection(Color.gray, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].modifier[0].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0]")
                    }
                    .frame(maxWidth: __designTimeSelection(CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[0].arg[0].value.arg[0].value", fallback: 60)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[0].arg[0].value"))
                    .offset(x: __designTimeSelection(CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 30)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[1].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1]")

                    __designTimeSelection(Spacer(), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[2]")
                    __designTimeSelection(Button(action: {
                        self.editorFeatures.undo = true
                        __designTimeSelection(print("\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].arg[0].value.[0].value", fallback: "SwiftUI UNDO "))\(__designTimeSelection(self.editorFeatures.undo, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))"), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1]")
                    }){
                        __designTimeSelection(Image(systemName: __designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[1].value.[0].arg[0].value.[0].value", fallback: "arrow.uturn.left")).font(.system(size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[1].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: 22))), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[1].value.[0]")
                    }
                    .offset(x: __designTimeSelection(CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].modifier[0].arg[0].value.arg[0].value", fallback: -10)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].modifier[0].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3]")
                    __designTimeSelection(Button(action: {
                        self.editorFeatures.redo = true
                        __designTimeSelection(print("\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1].arg[0].value.[0].value", fallback: "SwiftUI REDO "))\(__designTimeSelection(self.editorFeatures.redo, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))"), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1]")
                    }){
                        __designTimeSelection(Image(systemName: __designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[1].value.[0].arg[0].value.[0].value", fallback: "arrow.uturn.right")).font(.system(size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[1].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: 22))), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[1].value.[0]")
                    }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4]")
                }
                .offset(x: __designTimeSelection(CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: -15)), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].modifier[0].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0]")
                __designTimeSelection(HandwrittingViewController(title: __designTimeSelection(self.title, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value"), delegate: __designTimeSelection(self, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[1].value")).environmentObject(__designTimeSelection(editorFeatures, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].modifier[0].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1]")
            }
            .navigationBarTitle(__designTimeSelection(self.title, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].modifier[0].arg[0].value"))
            .navigationBarItems(trailing:
                                __designTimeSelection(Button(action: {
                                    self.editorFeatures.convert = true
                                }){
                                    __designTimeSelection(Text(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].modifier[1].arg[0].value.arg[1].value.[0].arg[0].value.[0].value", fallback: "Convert")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0].modifier[1].arg[0].value.arg[1].value.[0]")
                                }, "#5527.[1].[5].property.[0].[0].arg[0].value.[0].modifier[1].arg[0].value")), "#5527.[1].[5].property.[0].[0].arg[0].value.[0]")
            
            
        }
        .environment(\.horizontalSizeClass, .compact), "#5527.[1].[5].property.[0].[0]"))
#sourceLocation()
    }
}

typealias MyHandwritingView = SwiftHandwritting.MyHandwritingView
typealias HandwrittingViewController = SwiftHandwritting.HandwrittingViewController
typealias MyHandwritingView_Previews = SwiftHandwritting.MyHandwritingView_Previews