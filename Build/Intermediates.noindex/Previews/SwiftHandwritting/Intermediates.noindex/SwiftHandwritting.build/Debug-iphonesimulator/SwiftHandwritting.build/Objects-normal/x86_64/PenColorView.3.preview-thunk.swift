@_private(sourceFile: "PenColorView.swift") import SwiftHandwritting
import SwiftUI
import SwiftUI

extension PenColorView_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/PenColorView.swift", line: 30)
        AnyView(__designTimeSelection(PenColorView(), "#5835.[2].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension PenColorView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/PenColorView.swift", line: 16)
        AnyView(__designTimeSelection(ZStack{
            __designTimeSelection(Circle()
                .fill(__designTimeSelection(Color.white, "#5835.[1].[3].property.[0].[0].arg[0].value.[0].modifier[0].arg[0].value"))
                .frame(width: size+4, height: size+4), "#5835.[1].[3].property.[0].[0].arg[0].value.[0]")
            __designTimeSelection(Circle()
                .fill(__designTimeSelection(color, "#5835.[1].[3].property.[0].[0].arg[0].value.[1].modifier[0].arg[0].value"))
                .frame(width: __designTimeSelection(size, "#5835.[1].[3].property.[0].[0].arg[0].value.[1].modifier[1].arg[0].value"), height: __designTimeSelection(size, "#5835.[1].[3].property.[0].[0].arg[0].value.[1].modifier[1].arg[1].value")), "#5835.[1].[3].property.[0].[0].arg[0].value.[1]")
        }, "#5835.[1].[3].property.[0].[0]"))
#sourceLocation()
    }
}

typealias PenColorView = SwiftHandwritting.PenColorView
typealias PenColorView_Previews = SwiftHandwritting.PenColorView_Previews