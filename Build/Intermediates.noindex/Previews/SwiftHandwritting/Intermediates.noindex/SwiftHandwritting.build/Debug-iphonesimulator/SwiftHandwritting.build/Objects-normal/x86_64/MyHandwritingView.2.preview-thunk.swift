@_private(sourceFile: "MyHandwritingView.swift") import SwiftHandwritting
import SwiftUI
import SwiftUI

extension MyHandwritingView_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 280)
        AnyView(MyHandwritingView())
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
typealias Coordinator = HandwrittingViewController.Coordinator
typealias UIViewControllerType = HandwrittingViewController.UIViewControllerType

    @_dynamicReplacement(for: onError(_:blockId:message:)) private func __preview__onError(_ editor: IINKEditor, blockId: String, message: String) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 254)
            print(__designTimeString("#5527.[2].[8].[4].[0].arg[0].value.[0].value", fallback: "error"))
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
    @_dynamicReplacement(for: contentChanged(_:blockIds:)) private func __preview__contentChanged(_ editor: IINKEditor, blockIds: [String]) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 250)
            print(__designTimeString("#5527.[2].[8].[3].[0].arg[0].value.[0].value", fallback: "content"))
#sourceLocation()
    }
}

extension HandwrittingViewController.Coordinator {
    @_dynamicReplacement(for: partChanged(_:)) private func __preview__partChanged(_ editor: IINKEditor) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 246)
            print(__designTimeString("#5527.[2].[8].[2].[0].arg[0].value.[0].value", fallback: "part"))
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: makeCoordinator()) private func __preview__makeCoordinator() -> HandwrittingViewController.Coordinator {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 233)
        Coordinator(self)
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: updateUIViewController(_:context:)) private func __preview__updateUIViewController(_ editorViewController: EditorViewController, context: UIViewControllerRepresentableContext<HandwrittingViewController>) {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 189)
        if editorFeatures.undo {
            editorViewController.editor.undo()
            self.editorFeatures.undo = false
        }
        else if editorFeatures.redo {
            editorViewController.editor.redo()
            self.editorFeatures.redo = false
            
        }
        else if editorFeatures.convert {
            do {
                
                let supportedTargetStates = editorViewController.editor.getSupportedTargetConversionState(nil)
                try editorViewController.editor.convert(nil, targetState: supportedTargetStates[0].value)
//                self.delegate.delegate!.saveText(NSAttributedString(string: try editorViewController.editor.export_(nil, mimeType: .text)))
            }
            catch {
                
                print("Error while converting handwriting : " + error.localizedDescription)
                
            }
            self.editorFeatures.convert = false
        }
        
        editorViewController.inputMode = editorFeatures.touchType
        
        if editorFeatures.penColor == UIColor.black {
            editorViewController.editor.penStyle = "color: #000000FF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.blue {
            editorViewController.editor.penStyle = "color: #0000FFFF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.red {
            editorViewController.editor.penStyle = "color: #FF0000FF; -myscript-pen-width: 1.0"
        }
        else if editorFeatures.penColor == UIColor.yellow {
            editorViewController.editor.penStyle = "color: #FFFF00FF; -myscript-pen-width: 1.0"
        }
        
        print(__designTimeString("#5527.[2].[6].[3].arg[0].value.[0].value", fallback: "updateUIViewController called"))
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: createPackage(packageName:)) private func __preview__createPackage(packageName: String) throws -> IINKContentPackage? {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 175)
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = (UIApplication.shared.delegate as? AppDelegate)?.engine {
            resultPackage = try engine.createPackage(fullPath.decomposedStringWithCanonicalMapping)
            
            // Add a blank page type Text Document
            if let part = try resultPackage?.createPart("Text") /* Options are : "Diagram", "Drawing", "Math", "Text Document", "Text" */ {
                           self.title = "Type: " + part.type
            }
        }
        return resultPackage
#sourceLocation()
    }
}

extension HandwrittingViewController {
    @_dynamicReplacement(for: makeUIViewController(context:)) private func __preview__makeUIViewController(context: UIViewControllerRepresentableContext<HandwrittingViewController>) -> EditorViewController {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 125)
        let editorViewController = EditorViewController()
        editorViewController.view.isUserInteractionEnabled = true
//        editorViewController.di
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if (appDelegate.engine == nil)
            {
                print("\(__designTimeString("#5527.[2].[4].[2].[0].[0].[0].[0].arg[0].value.[0].value", fallback: "Certificate error "))\(appDelegate.engineErrorMessage ?? "No ERROR")\(__designTimeString("#5527.[2].[4].[2].[0].[0].[0].[0].arg[0].value.[2].value", fallback: ""))")
            }
            editorViewController.engine = appDelegate.engine
        }

        // Configure the iink runtime environment
        let configurationPath = Bundle.main.bundlePath.appending(__designTimeString("#5527.[2].[4].[3].value.modifier[0].arg[0].value.[0].value", fallback: "/recognition-assets/conf"))
        do {
            try editorViewController.engine.configuration.setStringArray([configurationPath], forKey:"configuration-manager.search-path") // Tells the engine where to load the recognition assets from.
        } catch {
            print("Should not happen, please check your resources assets : " + error.localizedDescription)
        }
        
        
        // Set the temporary directory
        do {
            try editorViewController.engine.configuration.setString(NSTemporaryDirectory(), forKey: "content-package.temp-folder")
        } catch {
            print("Failed to set temporary folder: " + error.localizedDescription)
        }
        
        editorViewController.inputMode = self.editorFeatures.touchType
        
        do {
            if let package = try createPackage(packageName: "New") {
                let myText = NSAttributedString(string: "Hello, MyScript")
                
                
                try editorViewController.editor.part = package.getPartAt(0)
//                try editorViewController.editor.addBlock(.zero, type: "Text", mimeType: .text, data: "Test")
                try editorViewController.editor.import_(IINKMimeType.text, data: myText.string, block: nil)
            }
        } catch {
            print("Error while creating package : " + error.localizedDescription)
        }
        
        return editorViewController
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: redo()) private func __preview__redo() -> Bool {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 114)
        return self.editorFeatures.redo
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: resetRedo()) private func __preview__resetRedo() {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 109)
        self.editorFeatures.redo = false
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: undo()) private func __preview__undo() -> Bool {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 104)
        return self.editorFeatures.undo
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: resetUndo()) private func __preview__resetUndo() {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 99)
        self.editorFeatures.undo = false
#sourceLocation()
    }
}

extension MyHandwritingView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/SwiftHandwritting/MyHandwritingView.swift", line: 19)
        AnyView(NavigationView {
            VStack {
                HStack{
                    Picker(selection: self.$editorFeatures.touchValue, label: Text(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].arg[1].value.arg[0].value.[0].value", fallback: "pick"))) {
                        ForEach(0 ..< self.touchTypes.count) {
                            Text(self.touchTypes[$0])
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    .fixedSize()
                        .offset(x: CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].modifier[2].arg[0].value.arg[0].value", fallback: 30)))
                    
//                    Picker(selection: self.$editorFeatures.penColorValue, label: Text("pick")) {
//                        ForEach(0 ..< self.penColors.count) {
//                            PenColorView(color: self.penColors[$0], size: 12)
//                        }
//                    }.pickerStyle(SegmentedPickerStyle())
//                    .fixedSize()
//                    .offset(x: CGFloat(30))
                    ScrollView(.horizontal){
                        HStack(spacing: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value", fallback: 10)) {
                            Button(action: {
                                self.editorFeatures.penColorValue = 0
                            }){
                                PenColorView(color: self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: 0)], size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[1].value", fallback: 12))
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 1
                            }){
                                PenColorView(color: self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: 1)], size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[1].arg[1].value.[0].arg[1].value", fallback: 12))
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 2
                            }){
                                PenColorView(color: self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: 2)], size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[1].value", fallback: 12))
                            }
                            Button(action: {
                                self.editorFeatures.penColorValue = 3
                            }){
                                PenColorView(color: self.penColors[__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0].arg[0].value.[0].value", fallback: 3)], size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[3].arg[1].value.[0].arg[1].value", fallback: 12))
                            }
                            
                        }
                        .background(Color.gray)
                    }
                    .frame(maxWidth: CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[0].arg[0].value.arg[0].value", fallback: 60)))
                    .offset(x: CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 30)))

                    Spacer()
                    Button(action: {
                        self.editorFeatures.undo = true
                        print("\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].arg[0].value.[0].value", fallback: "SwiftUI UNDO "))\(self.editorFeatures.undo)\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))")
                    }){
                        Image(systemName: __designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[1].value.[0].arg[0].value.[0].value", fallback: "arrow.uturn.left")).font(.system(size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].arg[1].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: 22)))
                    }
                    .offset(x: CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[3].modifier[0].arg[0].value.arg[0].value", fallback: -10)))
                    Button(action: {
                        self.editorFeatures.redo = true
                        print("\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1].arg[0].value.[0].value", fallback: "SwiftUI REDO "))\(self.editorFeatures.redo)\(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))")
                    }){
                        Image(systemName: __designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[1].value.[0].arg[0].value.[0].value", fallback: "arrow.uturn.right")).font(.system(size: __designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[4].arg[1].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: 22)))
                    }
                }
                .offset(x: CGFloat(__designTimeInteger("#5527.[1].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].modifier[0].arg[0].value.arg[0].value", fallback: -15)))
                HandwrittingViewController(title: self.title, delegate: self).environmentObject(editorFeatures)
            }
            .navigationBarTitle(self.title)
            .navigationBarItems(trailing:
                                Button(action: {
                                    self.editorFeatures.convert = true
                                }){
                                    Text(__designTimeString("#5527.[1].[5].property.[0].[0].arg[0].value.[0].modifier[1].arg[0].value.arg[1].value.[0].arg[0].value.[0].value", fallback: "Convert"))
                                })
            
            
        }
        .environment(\.horizontalSizeClass, .compact))
#sourceLocation()
    }
}

typealias MyHandwritingView = SwiftHandwritting.MyHandwritingView
typealias HandwrittingViewController = SwiftHandwritting.HandwrittingViewController
typealias MyHandwritingView_Previews = SwiftHandwritting.MyHandwritingView_Previews