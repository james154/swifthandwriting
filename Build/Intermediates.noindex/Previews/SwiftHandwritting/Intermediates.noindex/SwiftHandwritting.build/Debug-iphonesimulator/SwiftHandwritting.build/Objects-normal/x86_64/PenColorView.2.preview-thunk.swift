@_private(sourceFile: "PenColorView.swift") import SwiftHandwritting
import SwiftUI
import SwiftUI

extension PenColorView_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/PenColorView.swift", line: 30)
        AnyView(PenColorView())
#sourceLocation()
    }
}

extension PenColorView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/james/Documents/interactive-ink-examples-ios/Examples/SwiftHandwritting/PenColorView.swift", line: 16)
        AnyView(ZStack{
            Circle()
                .fill(Color.white)
                .frame(width: size+4, height: size+4)
            Circle()
                .fill(color)
                .frame(width: size, height: size)
        })
#sourceLocation()
    }
}

typealias PenColorView = SwiftHandwritting.PenColorView
typealias PenColorView_Previews = SwiftHandwritting.PenColorView_Previews